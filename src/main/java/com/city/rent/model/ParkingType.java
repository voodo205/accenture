package com.city.rent.model;

public enum ParkingType {
    BIKE,
    SCOOTER
}
