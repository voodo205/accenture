package com.city.rent.model;

public enum VehicleCondition {
    GOOD,
    SATISFACTORY,
    BAD
}
