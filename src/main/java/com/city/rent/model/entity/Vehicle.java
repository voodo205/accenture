package com.city.rent.model.entity;

import com.city.rent.model.VehicleStatus;
import lombok.*;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "vehicle")
public class Vehicle {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column(name = "identity_number")
    private String identityNumber;

    @Column(name = "condition")
    private String condition;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "type")
    private VehicleType type;

    @Column(name = "charge")
    private Integer charge;

    @Column(name = "max_speed")
    private Integer maxSpeed;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parking_id")
    private Parking parking;

    @Column(name = "status")
    private VehicleStatus status;
}
