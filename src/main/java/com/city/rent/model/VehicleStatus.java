package com.city.rent.model;

public enum VehicleStatus {
    BUSY,
    FREE
}
