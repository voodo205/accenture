package com.city.rent.model;

public enum RentStatus {
    CREATED,
    COMPLETED
}
