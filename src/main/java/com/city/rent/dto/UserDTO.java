package com.city.rent.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class UserDTO {
    private Long id;
    private String login;
    private BigDecimal balance;
    private Integer isAdmin;
}
