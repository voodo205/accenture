package com.city.rent.dto;

import lombok.Data;

@Data
public class ParkingDTO {
    private Long id;
    private String name;
    private Double radius;
    private String type;
    private Double latitude;
    private Double longitude;
}
