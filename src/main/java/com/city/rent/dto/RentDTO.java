package com.city.rent.dto;

import com.city.rent.model.entity.User;
import com.city.rent.model.entity.Vehicle;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class RentDTO {
    private Long id;
    private User user;
    private Vehicle vehicle;
    private Date startRentTime;
    private Date endRentTime;
    private String status;
    private BigDecimal fullCost;
    private Integer startPoint;
    private Integer endPoint;
}
