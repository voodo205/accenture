package com.city.rent.dto;

import com.city.rent.model.entity.Parking;
import com.city.rent.model.entity.VehicleType;
import lombok.Data;

@Data
public class VehicleDTO {
    private Long id;
    private Integer identityNumber;
    private VehicleType type;
    private String condition;
    private Parking parking;
    private Integer status;
}
