package com.city.rent.service;

import com.city.rent.dto.VehicleDTO;
import com.city.rent.model.entity.Vehicle;

import java.util.List;

public interface VehicleService {
    List<Vehicle> getVehicles();
    VehicleDTO getVehicle(Long vehicleId);
    void deleteVehicle(Long vehicleId);
    void updateVehicle(Vehicle vehicle);
    VehicleDTO createVehicle(Vehicle vehicle);
    List<Vehicle> findVehiclesByStatus(Integer status);
    List<Vehicle> findVehiclesByType(String type);
    List<Vehicle> findVehiclesByParkingId(Long id);
}
