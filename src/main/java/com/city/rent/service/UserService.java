package com.city.rent.service;

import com.city.rent.dto.UserDTO;
import com.city.rent.model.entity.User;

import java.util.List;

public interface UserService {
    List<User> getUsers();
    UserDTO getUser(Long userId);
    void deleteUser(Long userId);
    void updateUser(User user);
    void createUser(User user);
}
