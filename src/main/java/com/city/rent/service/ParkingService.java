package com.city.rent.service;

import com.city.rent.dto.ParkingDTO;
import com.city.rent.model.entity.Parking;

import java.util.List;

public interface ParkingService {
    List<Parking> getParkingList();
    Parking getParking(Long parkingId);
    void deleteParking(Long parkingId);
    Parking updateParking(Parking parking);
    Parking createParking(Parking parking);
}
