package com.city.rent.service;

import com.city.rent.dto.RentDTO;
import com.city.rent.model.entity.Rent;

import java.util.List;

public interface RentService {
    Integer RENT_LIMIT = 3;
    List<Rent> getRents();
    List<Rent> getRentsByUser(Long userId);
    RentDTO getRent(Long rentId);
    void deleteRent(Long rentId);
    void updateRent(Rent rent);
    RentDTO createRent(Rent rent);
    void closeRent(Rent closeRent);
    List<Rent> getRentsByUserId(Long id);
}
