package com.city.rent.service.impl;

import com.city.rent.dto.RentDTO;
import com.city.rent.model.entity.Rent;
import com.city.rent.repository.exception.RentNotFoundException;
import com.city.rent.repository.exception.UserBalanceLimitException;
import com.city.rent.repository.exception.UserRentLimitException;
import com.city.rent.repository.RentRepository;
import com.city.rent.service.RentService;
import com.city.rent.model.RentStatus;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class RentServiceImpl implements RentService {

    private final RentRepository repository;

    public RentServiceImpl(RentRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Rent> getRents() {
        return repository.findAll();
    }

    @Override
    public List<Rent> getRentsByUser(Long id) {
        return repository.findRentsByUser(id);
    }

    @Override
    public RentDTO getRent(Long id) {
        Rent rent = repository.findById(id)
                .orElseThrow(() -> new RentNotFoundException(id));
        RentDTO rentDTO = new RentDTO();
        BeanUtils.copyProperties(rent, rentDTO);
        return rentDTO;
    }

    @Override
    public void deleteRent(Long id) {
        repository.deleteById(id);
    }

    @Override
    public void updateRent(Rent newRent) {
        String newRentStatus = String.valueOf(newRent.getStatus());
        if (newRentStatus.equals(String.valueOf(RentStatus.COMPLETED)) &&
                repository.getById(newRent.getId()).getStatus().equals(String.valueOf(RentStatus.CREATED))) {

            long startTime = newRent.getStartRentTime().getTime();
            long endTime = newRent.getEndRentTime().getTime();

            BigDecimal minuteRentCost = newRent.getVehicle().getType().getMinuteRentCost();
            BigDecimal rentTimeMinutes = BigDecimal.valueOf((endTime - startTime) / 60);
            BigDecimal startRentCost = newRent.getVehicle().getType().getStartRentCost();

            BigDecimal fullCost = startRentCost.add(minuteRentCost.multiply(rentTimeMinutes));
            newRent.setFullCost(fullCost);

            if (fullCost.compareTo(newRent.getUser().getBalance()) > 0) {
                newRent.setStatus(RentStatus.valueOf(String.valueOf(RentStatus.CREATED)));
            }
        }

        repository.findById(newRent.getId())
                .map(rent -> {
                    rent.setUser(newRent.getUser());
                    rent.setVehicle(newRent.getVehicle());
                    rent.setStartRentTime(newRent.getStartRentTime());
                    rent.setEndRentTime(newRent.getEndRentTime());
                    rent.setStatus(RentStatus.valueOf(newRentStatus));
                    rent.setFullCost(newRent.getFullCost());
                    rent.setStartPoint(newRent.getStartPoint());
                    if (newRentStatus.equals(String.valueOf(RentStatus.COMPLETED))) {
                        rent.setStatus(RentStatus.valueOf(newRentStatus));
                    }
                    rent.setEndPoint(newRent.getEndPoint());
                    return repository.save(rent);
                });
    }

    @Override
    public RentDTO createRent(Rent newRent) {
        BigDecimal userBalance = newRent.getUser().getBalance();
        BigDecimal startRentCost = newRent.getVehicle().getType().getStartRentCost();

        Long userId = newRent.getUser().getId();

        if (repository.findRentsCountForUser(userId) > RentService.RENT_LIMIT) {
            throw new UserRentLimitException(userId);
        }
        if (userBalance.compareTo(startRentCost) > 0) {
            Rent rent = repository.save(newRent);
            RentDTO rentDTO = new RentDTO();
            BeanUtils.copyProperties(rent, rentDTO);
            return rentDTO;
        } else {
            throw new UserBalanceLimitException(userId, userBalance);
        }
    }

    @Override
    public void closeRent(Rent closeRent) {
        updateRent(closeRent);
    }

    @Override
    public List<Rent> getRentsByUserId(Long id) {
        return repository.findRentsByUser(id);
    }
}
