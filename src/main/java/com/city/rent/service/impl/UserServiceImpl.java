package com.city.rent.service.impl;

import com.city.rent.dto.UserDTO;
import com.city.rent.model.entity.User;
import com.city.rent.repository.exception.UserNotFoundException;
import com.city.rent.repository.UserRepository;
import com.city.rent.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository repository;

    public UserServiceImpl(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<User> getUsers() {
        return repository.findAll();
    }

    @Override
    public UserDTO getUser(Long id) {
        User user = repository.findById(id)
                .orElseThrow(() -> new UserNotFoundException(id));
        UserDTO userDTO = new UserDTO();
        BeanUtils.copyProperties(user, userDTO);
        return userDTO;
    }

    @Override
    public void deleteUser(Long id) {
        repository.deleteById(id);
    }

    @Override
    public void updateUser(User newUser) {
        repository.findById(newUser.getId())
                .map(user -> {
                    user.setLogin(newUser.getLogin());
                    user.setPassword(newUser.getPassword());
                    user.setBalance(newUser.getBalance());
                    user.setIsAdmin(newUser.getIsAdmin());
                    return repository.save(user);
                });
    }

    @Override
    public void createUser(User user) {
        repository.save(user);
    }
}
