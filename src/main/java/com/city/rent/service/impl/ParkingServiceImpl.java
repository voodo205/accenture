package com.city.rent.service.impl;

import com.city.rent.dto.ParkingDTO;
import com.city.rent.model.entity.Parking;
import com.city.rent.repository.exception.ParkingNotFoundException;
import com.city.rent.repository.ParkingRepository;
import com.city.rent.service.ParkingService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ParkingServiceImpl implements ParkingService {

    private final ParkingRepository repository;

    public ParkingServiceImpl(ParkingRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Parking> getParkingList() {
        return repository.findAll();
    }

    @Override
    public Parking getParking(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new ParkingNotFoundException(id));
    }

    @Override
    public void deleteParking(Long id) {
        repository.deleteById(id);
    }

    @Override
    public Parking updateParking(Parking newParking) {
        return repository.findById(newParking.getId())
                .map(parking -> {
                    parking.setName(newParking.getName());
                    parking.setLatitude(newParking.getLatitude());
                    parking.setLongitude(newParking.getLongitude());
                    parking.setRadius(newParking.getRadius());
                    parking.setType(newParking.getType());
                    return repository.save(parking);
                }).get();
    }

    @Override
    public Parking createParking(Parking newParking) {
        return repository.save(newParking);
    }
}
