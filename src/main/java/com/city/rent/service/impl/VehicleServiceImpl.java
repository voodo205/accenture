package com.city.rent.service.impl;

import com.city.rent.dto.VehicleDTO;
import com.city.rent.model.entity.Vehicle;
import com.city.rent.repository.exception.VehicleNotFoundException;
import com.city.rent.repository.VehicleRepository;
import com.city.rent.service.VehicleService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VehicleServiceImpl implements VehicleService {

    private final VehicleRepository repository;

    public VehicleServiceImpl(VehicleRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Vehicle> getVehicles() {
        return repository.findAll();
    }

    @Override
    public VehicleDTO getVehicle(Long id) {
        Vehicle vehicle = repository.findById(id)
                .orElseThrow(() -> new VehicleNotFoundException(id));
        VehicleDTO vehicleDTO = new VehicleDTO();
        BeanUtils.copyProperties(vehicle, vehicleDTO);
        return vehicleDTO;
    }

    @Override
    public void deleteVehicle(Long id) {
        repository.deleteById(id);
    }

    @Override
    public void updateVehicle(Vehicle newVehicle) {
        repository.findById(newVehicle.getId())
                .map(vehicle -> {
                    vehicle.setIdentityNumber(newVehicle.getIdentityNumber());
                    vehicle.setCondition(newVehicle.getCondition());
                    vehicle.setType(newVehicle.getType());
                    vehicle.setCharge(newVehicle.getCharge());
                    vehicle.setMaxSpeed(newVehicle.getMaxSpeed());
                    vehicle.setParking(newVehicle.getParking());
                    vehicle.setStatus(newVehicle.getStatus());
                    return repository.save(vehicle);
                });
    }

    @Override
    public VehicleDTO createVehicle(Vehicle newVehicle) {
        Vehicle vehicle = repository.save(newVehicle);
        VehicleDTO vehicleDTO = new VehicleDTO();
        BeanUtils.copyProperties(vehicle, vehicleDTO);
        return vehicleDTO;
    }

    @Override
    public List<Vehicle> findVehiclesByStatus(Integer status) {
        return repository.findByStatus(status);
    }

    @Override
    public List<Vehicle> findVehiclesByType(String type) {
        return repository.findByType(type);
    }

    @Override
    public List<Vehicle> findVehiclesByParkingId(Long id) {
        return repository.findByParkingId(id);
    }
}
