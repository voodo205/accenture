package com.city.rent.repository;

import com.city.rent.model.entity.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VehicleRepository extends JpaRepository<Vehicle, Long> {

    @Query(value = "SELECT * FROM VEHICLE WHERE TYPE = :type", nativeQuery = true)
    List<Vehicle> findByType(String type);

    @Query(value = "SELECT * FROM VEHICLE WHERE PARKING_ID = :id", nativeQuery = true)
    List<Vehicle> findByParkingId(Long id);

    @Query(value = "SELECT * FROM VEHICLE WHERE STATUS = :status", nativeQuery = true)
    List<Vehicle> findByStatus(Integer status);
}
