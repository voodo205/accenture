package com.city.rent.repository.exception;

import java.math.BigDecimal;

public class UserBalanceLimitException extends RuntimeException{
    public UserBalanceLimitException(Long id, BigDecimal balance) {
        super("User: " + id + " does not have sufficient balance: " + balance);
    }
}
