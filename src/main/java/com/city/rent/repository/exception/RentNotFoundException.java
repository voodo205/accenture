package com.city.rent.repository.exception;

public class RentNotFoundException extends RuntimeException{
    public RentNotFoundException(Long id) {
        super("Could not find rent " + id);
    }
}
