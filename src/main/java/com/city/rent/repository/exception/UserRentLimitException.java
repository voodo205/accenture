package com.city.rent.repository.exception;

import com.city.rent.service.RentService;

public class UserRentLimitException extends RuntimeException{
    public UserRentLimitException(Long id) {
        super("User could not have limit over " + RentService.RENT_LIMIT + " rent " + id);
    }
}
