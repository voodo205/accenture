package com.city.rent.repository;

import com.city.rent.model.entity.Rent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RentRepository extends JpaRepository<Rent, Long> {

    @Query(value = "SELECT COUNT(*) FROM RENT WHERE USER_ID = :id", nativeQuery = true)
    Integer findRentsCountForUser(Long id);

    @Query(value = "SELECT * FROM Rent WHERE USER_Id = :id", nativeQuery = true)
    List<Rent> findRentsByUser(Long id);
}
