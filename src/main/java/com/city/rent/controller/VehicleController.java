package com.city.rent.controller;

import com.city.rent.dto.VehicleDTO;
import com.city.rent.model.entity.Vehicle;
import com.city.rent.service.VehicleService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/vehicles")
public class VehicleController {

    private final VehicleService service;

    public VehicleController(VehicleService service) {
        this.service = service;
    }

    @PostMapping("/admin/")
    public ResponseEntity<VehicleDTO> createVehicle(@RequestBody Vehicle newVehicle) {
        return ResponseEntity.ok(service.createVehicle(newVehicle));
    }

    @DeleteMapping("/admin/{id}")
    public void deleteVehicle(@PathVariable Long id) {
        service.deleteVehicle(id);
    }

    @GetMapping("/status/{status}")
    public List<Vehicle> getVehiclesByStatus(@PathVariable Integer status) {
        return service.findVehiclesByStatus(status);
    }

    @GetMapping("/type/{type}")
    public List<Vehicle> getVehiclesByType(@PathVariable String type) {
        return service.findVehiclesByType(type);
    }

    @GetMapping("/parking/{id}")
    public List<Vehicle> getVehiclesByParkingId(@PathVariable Long id) {
        return service.findVehiclesByParkingId(id);
    }
}
