package com.city.rent.controller;

import com.city.rent.dto.UserDTO;
import com.city.rent.repository.exception.UserNotFoundException;
import com.city.rent.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/users")
public class UserController {

    private final UserService service;

    public UserController(UserService userService) {
        this.service = userService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDTO> getUser(@PathVariable Long id) {
        UserDTO userDTO;
        try {
            userDTO = service.getUser(id);
        } catch (UserNotFoundException exception) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(userDTO);
    }
}
