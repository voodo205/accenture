package com.city.rent.controller;

import com.city.rent.dto.RentDTO;
import com.city.rent.model.entity.Rent;
import com.city.rent.repository.exception.RentNotFoundException;
import com.city.rent.service.RentService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rents")
public class RentController {

    private final RentService service;

    public RentController(RentService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public ResponseEntity<RentDTO> getRent(@PathVariable Long id) {
        RentDTO rentDTO;
        try {
            rentDTO = service.getRent(id);
        } catch (RentNotFoundException exception) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(rentDTO);
    }

    @PostMapping("/{id}")
    public ResponseEntity<RentDTO> openRent(@RequestBody Rent newRent) {
        return ResponseEntity.ok(service.createRent(newRent));
    }

    @PostMapping("/rent/close/{id}")
    public void closeRent(@RequestBody Rent closeRent) {
       service.closeRent(closeRent);
    }

    @GetMapping("/rent/open/{id}")
    public List<Rent> getRentsByUserId(@RequestBody Long id) {
        return service.getRentsByUserId(id);
    }
}
