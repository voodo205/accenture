package com.city.rent.controller;

import com.city.rent.dto.ParkingDTO;
import com.city.rent.model.entity.Parking;
import com.city.rent.service.ParkingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/parking")
public class ParkingController {

    private final ParkingService service;

    public ParkingController(ParkingService service) {
        this.service = service;
    }

    @PostMapping("/admin/")
    public ResponseEntity<Parking> createParking(@RequestBody Parking newParking) {
        return ResponseEntity.ok(service.createParking(newParking));
    }

    @PutMapping("/admin/{id}")
    public ResponseEntity<Parking> updateParking(@RequestBody Parking newParking, @PathVariable Long id) {
        return ResponseEntity.ok(service.updateParking(newParking));
    }

    @DeleteMapping("/admin/{id}")
    public void deleteParking(@PathVariable Long id) {
        service.deleteParking(id);
    }

    @GetMapping("/")
    public List<Parking> getAllParking() {
        return service.getParkingList();
    }
}
